# Fintonic Challenge

Prueba técnica para la vacante de Desarrollador iOS

La app MarvelHeroes muestra una lista de súper héroes, la cual es obtenida a partir de una api. 
Al hacer tap sobre cualquiera de los elementos mostrados se puede acceder a los detalles del mismo.

El proyecto utiliza Pods por lo que antes de abrirlo es necesario ir a la terminal y ejecutar el comando pod install. 
Una vez hecho esto hay que abrir el archivo con extensión "workspace".

*  Se utilizó una arquitectura simple MVC ya que se trata de un proyecto con una complejidad baja.

*  Se hizo uso de algunas librerías para el mapeo de objetos de los cuales se dervian los modelos de datos, para el manejo de imágenes con web caché y para aplicar un activity indicator estilizado.

*  Se agregó la opción de hacer pull to refresh a la lista de súper héroes en caso de que sea necesario actualizar los valores.

*  El consumo de servicios se manejó de forma nativa a través de una clase independiente que utiliza URLSession, mediante un método que recibe la url y el método que se utilizará. De igual forma la respuesta se manejó por medio de un protocolo que indica si hubo éxtio o fracaso y devuelve los datos necesarios.

