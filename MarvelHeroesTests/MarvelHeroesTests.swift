//
//  MarvelHeroesTests.swift
//  MarvelHeroesTests
//
//  Created by Boris Iruegas Bocardo on 11/30/19.
//  Copyright © 2019 Boris Iruegas Bocardo. All rights reserved.
//

import XCTest
@testable import MarvelHeroes

class MarvelHeroesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCharacterModel() {
        let jsonStr = "{\"name\": \"Iron Man\",\"photo\": \"https://i.annihil.us/u/prod/marvel/i/mg/6/a0/55b6a25e654e6/standard_xlarge.jpg\", \"realName\": \"Anthony Edward \"Tony\" Stark\", \"height\": \"1.85m\", \"power\": \"None; Tony's body had been enhanced by the modified techno-organic virus, Extremis, but it is currently inaccessible and inoperable.\", \"abilities\": \"Tony has a genius level intellect that allows him to invent a wide range of sophisticated devices, specializing in advanced weapons and armor. He possesses a keen business mind.\", \"groups\": \"The Avengers, Initiative, Hellfire Club (outer circle), S.H.I.E.L.D., Illuminati, Thunderbolts, Force Works, Queen's Vengeance, Alcoholics Anonymous\"}"
        
        if let jsonData = jsonStr.data(using: String.Encoding.utf8) {

            if let hero = try? JSONSerialization.jsonObject(with: jsonData) as? CharacterModel {
                let controller = CharacterDetailViewController()
                controller.heroe = hero
                
                XCTAssertEqual(hero.name, controller.nickname.text)
                
            } else {
                debugPrint("Error al serializar el json")
            }
        } else {
            debugPrint("Error al convertir a tipo data")
        }
    }
}
