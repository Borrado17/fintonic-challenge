//
//  NetworkManager.swift
//  MarvelHeroes
//
//  Created by Boris Iruegas Bocardo on 11/30/19.
//  Copyright © 2019 Boris Iruegas Bocardo. All rights reserved.
//

import UIKit

protocol NetworkManagerProtocol {
    func successResponse(data: Data)
    func failureResponse(error: String)
}

class NetworkManager: NSObject {
    
    var delegate : NetworkManagerProtocol?
    
    func getCharacters(url: String, method: String) {
        var request : URLRequest = URLRequest(url: URL(string: url)!)
        request.httpMethod = method
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")  // the request is JSON
        //request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { data, _, error -> Void in
            if error == nil
            {
                if data != nil {
                    self.delegate?.successResponse(data: data!)
                } else {
                    self.delegate?.failureResponse(error: "couldn't read data")
                }
            } else
            {
                self.delegate?.failureResponse(error: error!.localizedDescription)
            }
        })
        task.resume()
    }

}
