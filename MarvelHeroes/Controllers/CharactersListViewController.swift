//
//  CharactersListViewController.swift
//  MarvelHeroes
//
//  Created by Boris Iruegas Bocardo on 11/30/19.
//  Copyright © 2019 Boris Iruegas Bocardo. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class CharactersListViewController: UIViewController, NetworkManagerProtocol, NVActivityIndicatorViewable, UITableViewDelegate, UITableViewDataSource {

    var heroes :  [CharacterModel]?
    fileprivate var refreshControl : UIRefreshControl = UIRefreshControl()
    var activityView = UIView()
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var emptyStateView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.register(UINib(nibName: "CharacterTableViewCell", bundle: nil), forCellReuseIdentifier: "CharacterTableViewCell")
        
        self.refreshControl.tintColor = UIColor.clear
        self.refreshControl.addTarget(self, action: #selector(CharactersListViewController.getCharacters), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableview.refreshControl = self.refreshControl
        } else {
            tableview.addSubview(self.refreshControl)
        }
        tableview.bringSubviewToFront(self.refreshControl)
        
        self.getCharacters()
    }
    
    @objc func getCharacters() {
        //Se crea e inicia un loader para realizar la petición de servicio
        let rect = CGRect(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.midY, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: rect, type: .ballScaleRipple, color: .red, padding: 0)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        activityView = activityIndicatorView
        
        //Se hace la petición para obtener los datos que poblarán la tabla de personajes. Se agrega el delegado para poder obtener la respuesta posteriormente
        let network = NetworkManager()
        network.delegate = self
        network.getCharacters(url: "https://api.myjson.com/bins/bvyob", method: "GET")
    }
    
    @objc func hideLoader() {
        self.stopAnimating(nil)
        activityView.removeFromSuperview()
        self.refreshControl.endRefreshing()
    }
    
    func serializeData (data: Data) {
        do {
            let json = try JSONSerialization.jsonObject(with: data) as! Dictionary<String, Any>
            let response = CharacterResponse.init(JSON: json)
            if let characters = response?.characterArray {
                self.heroes = characters
                self.performSelector(onMainThread: #selector(populateTable), with: nil, waitUntilDone: false)
            }
        } catch {
            print("couldn't serialize data")
        }
    }
    
    @objc func populateTable() {
        self.tableview.reloadData()
        self.emptyStateView.isHidden = true
    }
    
    //MARK: - NetworkManagerProtocol methods
    func successResponse(data: Data) {
        self.performSelector(onMainThread: #selector(hideLoader), with: nil, waitUntilDone: false)
        self.serializeData(data: data)
    }
    
    func failureResponse(error: String) {
        self.performSelector(onMainThread: #selector(hideLoader), with: nil, waitUntilDone: false)
        let alert = UIAlertController(title: "Cuidado!", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - TableView Data Source methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if heroes != nil {
            return heroes!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CharacterTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell", for: indexPath) as! CharacterTableViewCell
        let heroe : CharacterModel = self.heroes![indexPath.row]
        
        cell.name.text = heroe.realName
        cell.nickname.text = heroe.name
        cell.photo.sd_setImage(with: URL(string: heroe.photoURL!), completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let heroe : CharacterModel = self.heroes![indexPath.row]
        let controller : CharacterDetailViewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CharacterDetailVC") as! CharacterDetailViewController
        controller.heroe = heroe
        
        self.present(controller, animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    
    //MARK: - TableView Delegate methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
