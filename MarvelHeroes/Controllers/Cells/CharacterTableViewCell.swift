//
//  CharacterTableViewCell.swift
//  MarvelHeroes
//
//  Created by Boris Iruegas Bocardo on 11/30/19.
//  Copyright © 2019 Boris Iruegas Bocardo. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.layer.borderWidth = 1.0
        self.bgView.layer.borderColor = UIColor.cyan.cgColor
        self.bgView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
