//
//  CharacterDetailViewController.swift
//  MarvelHeroes
//
//  Created by Boris Iruegas Bocardo on 11/30/19.
//  Copyright © 2019 Boris Iruegas Bocardo. All rights reserved.
//

import UIKit

class CharacterDetailViewController: UIViewController {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nickname: UILabel!
    @IBOutlet weak var realname: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var power: UITextView!
    @IBOutlet weak var abilities: UITextView!
    @IBOutlet weak var groups: UITextView!
    
    var heroe : CharacterModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.photo.layer.cornerRadius = self.photo.frame.height / 2
        self.realname.adjustsFontSizeToFitWidth = true
        self.abilities.isEditable = false
        self.power.isEditable = false
        self.groups.isEditable = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.abilities.setContentOffset(.zero, animated: false)
        self.power.setContentOffset(.zero, animated: false)
        self.groups.setContentOffset(.zero, animated: false)
    }
    
    func setupView() {
        self.photo.sd_setImage(with: URL(string: heroe!.photoURL!), completed: nil)
        self.nickname.text = heroe?.name
        self.realname.text = heroe?.realName
        self.height.text = heroe?.height
        self.power.text = heroe?.power
        self.abilities.text = heroe?.abilities
        self.groups.text = heroe?.groups
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
