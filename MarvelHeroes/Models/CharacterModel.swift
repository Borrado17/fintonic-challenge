//
//  CharacterModel.swift
//  MarvelHeroes
//
//  Created by Boris Iruegas Bocardo on 11/30/19.
//  Copyright © 2019 Boris Iruegas Bocardo. All rights reserved.
//

import ObjectMapper

class CharacterModel: Mappable {
    
    var name: String?
    var photoURL: String?
    var realName: String?
    var height: String?
    var power: String?
    var abilities: String?
    var groups: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        photoURL <- map["photo"]
        realName <- map["realName"]
        height <- map["height"]
        power <- map["power"]
        abilities <- map["abilities"]
        groups <- map["groups"]
    }
}

class CharacterResponse: Mappable {
    
    var characterArray: [CharacterModel]?
    
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        characterArray <- map["superheroes"]
    }
}
